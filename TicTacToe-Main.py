#TicTacToe created by Justin Parrigin.

#variable
numGrid = [1,2,3,4,5,6,7,8,9]
playerWon = False
curTurn = False

def PrintGrid(curTurn):
    print( f'  {numGrid[0]} | {numGrid[1]} | {numGrid[2]} \n' ,
           ' ---------- \n' ,
           f' {numGrid[3]} | {numGrid[4]} | {numGrid[5]} \n' ,
           ' ---------- \n' ,
           f' {numGrid[6]} | {numGrid[7]} | {numGrid[8]} \n')

    WinCheck(curTurn)


# Check if someone won

def WinCheck(curTurn):
    whoWon = 0

    # o Player One
    if (#1 2 3
        ((numGrid[0] == 'o') and (numGrid[1] == 'o') and (numGrid[2] == 'o'))
        or
        #1 4 7
        ((numGrid[0] == 'o') and (numGrid[3] == 'o') and (numGrid[6] == 'o'))
        or
        #7 8 9
        ((numGrid[6] == 'o') and (numGrid[7] == 'o') and (numGrid[8] == 'o'))
        or
        #3 6 9
        ((numGrid[2] == 'o') and (numGrid[5] == 'o') and (numGrid[8] == 'o'))
        or
        #2 5 8
        ((numGrid[1] == 'o') and (numGrid[4] == 'o') and (numGrid[7] == 'o'))
        or
        #4 5 6
        ((numGrid[3] == 'o') and (numGrid[4] == 'o') and (numGrid[5] == 'o'))
        or
        #1 5 9
        ((numGrid[0] == 'o') and (numGrid[4] == 'o') and (numGrid[8] == 'o'))
        or
        # 3 5 7
        ((numGrid[2] == 'o') and (numGrid[4] == 'o') and (numGrid[6] == 'o'))
        ):

        whoWon = 1
        PlayerWon(whoWon)

    # x Player Two
    elif (#1 2 3
        ((numGrid[0] == 'x') and (numGrid[1] == 'x') and (numGrid[2] == 'x'))
        or
        #1 4 7
        ((numGrid[0] == 'x') and (numGrid[3] == 'x') and (numGrid[6] == 'x'))
        or
        #7 8 9
        ((numGrid[6] == 'x') and (numGrid[7] == 'x') and (numGrid[8] == 'x'))
        or
        #3 6 9
        ((numGrid[2] == 'x') and (numGrid[5] == 'x') and (numGrid[8] == 'x'))
        or
        #2 5 8
        ((numGrid[1] == 'x') and (numGrid[4] == 'x') and (numGrid[7] == 'x'))
        or
        #4 5 6
        ((numGrid[3] == 'x') and (numGrid[4] == 'x') and (numGrid[5] == 'x'))
        or
        #1 5 9
        ((numGrid[0] == 'x') and (numGrid[4] == 'x') and (numGrid[8] == 'x'))
        or
        # 3 5 7
        ((numGrid[2] == 'x') and (numGrid[4] == 'x') and (numGrid[6] == 'x'))
        ):

        whoWon = 2
        PlayerWon(whoWon)

    elif (((numGrid.count('x') == 4) and (numGrid.count('o') == 5)) or ((numGrid.count('x') == 5) and (numGrid.count('o') == 4))):
        EnactTie()
        
    else:
        MakeMove(curTurn)

# Make a Move

def MakeMove(curTurn):
    
    if(curTurn == False):
        playerNum = int(input("Player One, make your move : "))

        if (((playerNum >= 1) and (playerNum <= 9)) and ((not numGrid[playerNum - 1] == 'x') and (not numGrid[playerNum - 1] == 'o'))):
            numGrid[playerNum - 1] = 'o'
            curTurn = True
            PrintGrid(curTurn)
        
        else:
            MakeMove(curTurn)
        
    elif (curTurn == True):
        playerNum = int(input("Player Two, make your move : "))

        if (((playerNum >= 1) and (playerNum <= 9)) and ((not numGrid[playerNum - 1] == 'x') and (not numGrid[playerNum - 1] == 'o'))):
            numGrid[playerNum - 1] = 'x'
            curTurn = False
            PrintGrid(curTurn)
        else:
            MakeMove(curTurn)


# Player Won

def PlayerWon(whoWon):
    if(whoWon == 1):
        print('Player One Won!!')
    else:
        print('Player Two Won!!')

# Tie

def EnactTie():
    print("Its a Tie!")

PrintGrid(curTurn)


